/*
    Copyright (C) 2017 by Kai Uwe Broulik <kde@privat.broulik.de>
    Copyright (C) 2017 by David Edmundson <davidedmundson@kde.org>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#include "settings.h"

#include <unistd.h> // getppid

#include <QGuiApplication>
#include <QDBusConnection>
#include <QProcess>

#include <KProcessList>

#include "pluginmanager.h"
#include "settingsadaptor.h"

#include <config-host.h>

const QMap<Settings::Environment, QString> Settings::environmentNames = {
    {Settings::Environment::Chrome, QStringLiteral("chrome")},
    {Settings::Environment::Chromium, QStringLiteral("chromium")},
    {Settings::Environment::Vivaldi, QStringLiteral("vivaldi")},
    {Settings::Environment::Brave, QStringLiteral("brave")},
    {Settings::Environment::Firefox, QStringLiteral("firefox")},
    {Settings::Environment::WaterfoxClassic, QStringLiteral("waterfox-classic")},
    {Settings::Environment::WaterfoxCurrent, QStringLiteral("waterfox-current")},
    {Settings::Environment::WaterfoxG3, QStringLiteral("waterfox-g3")},
    {Settings::Environment::Tor, QStringLiteral("tor")},
    {Settings::Environment::IceCat, QStringLiteral("icecat")},
    {Settings::Environment::Palemoon, QStringLiteral("palemoon")},
    {Settings::Environment::Basilisk, QStringLiteral("basilisk")},
    {Settings::Environment::LibreWolf, QStringLiteral("librewolf")},
    {Settings::Environment::Opera, QStringLiteral("opera")},
    {Settings::Environment::Edge, QStringLiteral("edge")},
};

const QMap<Settings::Environment, EnvironmentDescription> Settings::environmentDescriptions = {
    {Settings::Environment::Chrome, {
        QStringLiteral("google-chrome"),
        QStringLiteral("Google Chrome"),
        QStringLiteral("google-chrome"),
        QStringLiteral("google.com"),
        QStringLiteral("Google"),
        QStringLiteral("google-chrome")
    } },
    {Settings::Environment::Chromium, {
        QStringLiteral("chromium-browser"),
        QStringLiteral("Chromium"),
        QStringLiteral("chromium-browser"),
        QStringLiteral("google.com"),
        QStringLiteral("Google"),
        QStringLiteral("chromium-browser")
    } },
    {Settings::Environment::Firefox, {
        QStringLiteral("firefox"),
        QStringLiteral("Mozilla Firefox"),
        QStringLiteral("firefox"),
        QStringLiteral("mozilla.org"),
        QStringLiteral("Mozilla"),
        QStringLiteral("firefox")
    } },
    {Settings::Environment::WaterfoxClassic, {
        QStringLiteral("waterfox-classic"),
        QStringLiteral("Waterfox Classic"),
        QStringLiteral("waterfox-classic"),
        QStringLiteral("waterfox.net"),
        QStringLiteral("Waterfox"),
        QStringLiteral("waterfox-classic")
    } },
    {Settings::Environment::WaterfoxCurrent, {
        QStringLiteral("waterfox-current"),
        QStringLiteral("Waterfox Current"),
        QStringLiteral("waterfox-current"),
        QStringLiteral("waterfox.net"),
        QStringLiteral("Waterfox"),
        QStringLiteral("waterfox-current")
    } },
    {Settings::Environment::WaterfoxG3, {
        QStringLiteral("waterfox-g3"),
        QStringLiteral("Waterfox G3"),
        QStringLiteral("waterfox-g3"),
        QStringLiteral("waterfox.net"),
        QStringLiteral("Waterfox"),
        QStringLiteral("waterfox-g3")
    } },
    {Settings::Environment::IceCat, {
        QStringLiteral("icecat"),
        QStringLiteral("Icecat"),
        QStringLiteral("icecat"),
        QStringLiteral("gnu.org/software/gnuzilla/"),
        QStringLiteral("Free Software Foundation"),
        QStringLiteral("icecat")
    } },
    {Settings::Environment::Tor, {
        QStringLiteral("tor-browser"),
        QStringLiteral("Tor browser"),
        QStringLiteral("tor-browser"),
        QStringLiteral("torproject.org"),
        QStringLiteral("Tor Project"),
        QStringLiteral("tor-browser")
    } },
    {Settings::Environment::Palemoon, {
        QStringLiteral("palemoon"),
        QStringLiteral("Palemoon"),
        QStringLiteral("palemoon"),
        QStringLiteral("palemoon.org"),
        QStringLiteral("Moonchild Productions"),
        QStringLiteral("palemoon")
    } },
    {Settings::Environment::Basilisk, {
        QStringLiteral("basilisk"),
        QStringLiteral("Basilisk"),
        QStringLiteral("basilisk"),
        QStringLiteral("basilisk-browser.org"),
        QStringLiteral("Basilisk"),
        QStringLiteral("basilisk")
    } },
    {Settings::Environment::LibreWolf, {
        QStringLiteral("librewolf"),
        QStringLiteral("LibreWolf"),
        QStringLiteral("librewolf"),
        QStringLiteral("librewolf-community.gitlab.io"),
        QStringLiteral("LibreWolf"),
        QStringLiteral("librewolf")
    } },
    {Settings::Environment::Opera, {
        QStringLiteral("opera"),
        QStringLiteral("Opera"),
        QStringLiteral("opera"),
        QStringLiteral("opera.com"),
        QStringLiteral("Opera"),
        QStringLiteral("opera")
    } },
    {Settings::Environment::Vivaldi, {
        QStringLiteral("vivaldi"),
        QStringLiteral("Vivaldi"),
        // This is what the official package on their website uses
        QStringLiteral("vivaldi-stable"),
        QStringLiteral("vivaldi.com"),
        QStringLiteral("Vivaldi"),
        QStringLiteral("vivaldi")
    } },
    {Settings::Environment::Brave, {
        QStringLiteral("brave"),
        QStringLiteral("Brave"),
        //As appears in Arch. Could be brave-browser too?
        QStringLiteral("brave-browser"),
        QStringLiteral("brave.com"),
        QStringLiteral("Brave"),
        QStringLiteral("brave")
    } },
    {Settings::Environment::Edge, {
        QStringLiteral("microsoft-edge-dev"),
        QStringLiteral("Microsoft Edge"),
        QStringLiteral("microsoft-edge-dev"),
        QStringLiteral("microsoft.com"),
        QStringLiteral("Microsoft Corp"),
        QStringLiteral("microsoft-edge-dev")
    } }
};

Settings::Settings()
    : AbstractBrowserPlugin(QStringLiteral("settings"), 1, nullptr)
{
    new SettingsAdaptor(this);
    QDBusConnection::sessionBus().registerObject(QStringLiteral("/Settings"), this);

}

Settings &Settings::self()
{
    static Settings s_self;
    return s_self;
}

void Settings::handleData(const QString &event, const QJsonObject &data)
{
    if (event == QLatin1String("changed")) {
        m_settings = data;

        for (auto it = data.begin(), end = data.end(); it != end; ++it) {
            const QString &subsystem = it.key();
            const QJsonObject &settingsObject = it->toObject();

            const QJsonValue enabledVariant = settingsObject.value(QStringLiteral("enabled"));
            // probably protocol overhead, not a plugin setting, skip.
            if (enabledVariant.type() == QJsonValue::Undefined) {
                continue;
            }

            auto *plugin = PluginManager::self().pluginForSubsystem(subsystem);
            if (!plugin) {
                continue;
            }

            if (enabledVariant.toBool()) {
                PluginManager::self().loadPlugin(plugin);
            } else {
                PluginManager::self().unloadPlugin(plugin);
            }

            PluginManager::self().settingsChanged(plugin, settingsObject);
        }

        emit changed(data);
    } else if (event == QLatin1String("openKRunnerSettings")) {
        QProcess::startDetached(QStringLiteral("kcmshell5"), {QStringLiteral("kcm_plasmasearch")});
    } else if (event == QLatin1String("setEnvironment")) {
        QString name = data[QStringLiteral("browserName")].toString();

        // Most chromium-based browsers just impersonate Chromium nowadays to keep websites from locking them out
        // so we'll need to make an educated guess from our parent process
        if (name == QLatin1String("chromium") || name == QLatin1String("chrome")) {
            const auto processInfo = KProcessList::processInfo(getppid());
            if (processInfo.name().contains(QLatin1String("vivaldi"))) {
                name = QStringLiteral("vivaldi");
            } else if (processInfo.name().contains(QLatin1String("brave"))) {
                name = QStringLiteral("brave");
            } else if (processInfo.name().contains(QLatin1String("edge"))) {
                name = QStringLiteral("edge");
            }
        } else if (name == QLatin1String("firefox")){
            const auto processInfo = KProcessList::processInfo(getppid());
            if (processInfo.name().contains(QLatin1String("waterfox-classic"))) {
                name = QStringLiteral("waterfox-classic");
            } else if (processInfo.name().contains(QLatin1String("waterfox-current"))) {
                name = QStringLiteral("waterfox-current");
            } else if (processInfo.name().contains(QLatin1String("waterfox-g3"))) {
                name = QStringLiteral("waterfox-g3");
            } else if (processInfo.name().contains(QLatin1String("basilisk"))) {
                name = QStringLiteral("basilisk");
            } else if (processInfo.name().contains(QLatin1String("Palemoon"))) {
                name = QStringLiteral("palemoon");
            } else if (processInfo.name().contains(QLatin1String("librewolf"))) {
                name = QStringLiteral("librewolf");
            } else if (processInfo.name().contains(QLatin1String("tor"))) {
                name = QStringLiteral("tor");
            } else if (processInfo.name().contains(QLatin1String("icecat"))) {
                name = QStringLiteral("icecat");
            }
        }

        m_environment = Settings::environmentNames.key(name, Settings::Environment::Unknown);
        m_currentEnvironment = Settings::environmentDescriptions.value(m_environment);

        qApp->setApplicationName(m_currentEnvironment.applicationName);
        qApp->setApplicationDisplayName(m_currentEnvironment.applicationDisplayName);
        qApp->setDesktopFileName(m_currentEnvironment.desktopFileName);
        qApp->setOrganizationDomain(m_currentEnvironment.organizationDomain);
        qApp->setOrganizationName(m_currentEnvironment.organizationName);
    }
}

QJsonObject Settings::handleData(int serial, const QString &event, const QJsonObject &data)
{
    Q_UNUSED(serial)
    Q_UNUSED(data)

    QJsonObject ret;

    if (event == QLatin1String("getSubsystemStatus")) {
       // should we add a PluginManager::knownSubsystems() that returns a QList<AbstractBrowserPlugin*>?
       const QStringList subsystems = PluginManager::self().knownPluginSubsystems();
       for (const QString &subsystem : subsystems) {
           const AbstractBrowserPlugin *plugin = PluginManager::self().pluginForSubsystem(subsystem);

           QJsonObject details = plugin->status();
           details.insert(QStringLiteral("version"), plugin->protocolVersion());
           details.insert(QStringLiteral("loaded"), plugin->isLoaded());
           ret.insert(subsystem, details);
        }
    } else if (event == QLatin1String("getVersion")) {
        ret.insert(QStringLiteral("host"), QStringLiteral(HOST_VERSION_STRING));
    }

    return ret;
}

Settings::Environment Settings::environment() const
{
    return m_environment;
}

QString Settings::environmentString() const
{
    return Settings::environmentNames.value(m_environment);
}

EnvironmentDescription Settings::environmentDescription() const
{
    return m_currentEnvironment;
}

bool Settings::pluginEnabled(const QString &subsystem) const
{
    return settingsForPlugin(subsystem).value(QStringLiteral("enabled")).toBool();
}

QJsonObject Settings::settingsForPlugin(const QString &subsystem) const
{
    return m_settings.value(subsystem).toObject();
}
